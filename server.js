const express = require("express");
const app = express();
const path = require("path");

app.set('view engine', 'ejs');
app.set('views', path.join(__dirname, 'views'));

app.use('/assets', express.static('assets'));

app.get("/", (req,res) => {
    res.render('index', {});
})

app.get("/contact-us", (req,res) => {
    res.render('contactus', {});
})

app.listen(9000, () => {
    console.log('Running on port 9000')
})